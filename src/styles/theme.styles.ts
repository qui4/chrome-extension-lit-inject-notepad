import { css } from 'lit';

export const primary = css`#363636`;
export const secondary = css`#425795`;
export const background = css`#242424`;
export const foreground = css`#f9f9f9`;
