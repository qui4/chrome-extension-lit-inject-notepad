import { LitElement, SVGTemplateResult, TemplateResult } from 'lit';
import { Size } from '../elements/root-element/window-element/models/Size';

export type Window = {
  id: string;
  title?: string;
  icon: SVGTemplateResult;
  scopedElements?: { [key: string]: LitElement & any };
  template: string | TemplateResult;
  visible: boolean;
  selected?: boolean;
  minimized?: boolean;
  resizable?: boolean;
  minSize?: Size;
};
