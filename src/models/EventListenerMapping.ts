export type EventListenerMapping = {
  target: Element | Window;
  type: string;
  handler: () => any; // eslint-disable-line @typescript-eslint/no-explicit-any
};
