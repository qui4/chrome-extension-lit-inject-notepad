const PREFIX = 'chrome-extension';

export const storage = {
  setItem: (key: string, data: string | boolean): void =>
    localStorage.setItem(`${PREFIX}:${key}`, data.toString()),
  getItem: (key: string): string | boolean | null => {
    const value = localStorage.getItem(`${PREFIX}:${key}`);
    if (value === 'true') return true;
    if (value === 'false') return false;
    return value;
  },
  removeItem: (key: string): void =>
    localStorage.removeItem(`${PREFIX}:${key}`),
};
