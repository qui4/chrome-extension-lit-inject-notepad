import { ScopedElementsMixin } from '@open-wc/scoped-elements';
import { LitElement, html, css, PropertyValues, nothing } from 'lit';
import { property, state } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { Window } from '../../../models/Window';
import {
  materialBoxShadow1,
  materialBoxShadow2,
} from '../../../styles/fancy.styles';
import { background, primary, secondary } from '../../../styles/theme.styles';
import { windows } from '../../windows';
import { SvgIcon } from '../svg-icon';

export class TaskbarElement extends ScopedElementsMixin(LitElement) {
  @property() public windows!: Window[];
  @state() private _windows: Window[] = [];

  static get scopedElements() {
    return {
      'svg-icon': SvgIcon,
    };
  }

  static override styles = css`
    :host {
      position: absolute;
      bottom: 12px;
      right: 0px;
    }

    section {
      display: flex;
      flex-direction: row-reverse;
      background: ${background};
      align-items: center;
      padding: 12px;
      border-radius: 30px 0 0 30px;
      box-shadow: ${materialBoxShadow1};
    }

    button {
      border: none;
      outline: none;
      padding: 12px;
      background: ${primary};
      width: 40px;
      height: 40px;
      border-radius: 50%;
      box-shadow: ${materialBoxShadow1};
    }

    button:not(:first-child) {
      margin-right: 12px;
    }

    button[selected] {
      background: ${secondary};
      box-shadow: ${materialBoxShadow2};
    }

    button[minimized] {
      background: transparent;
      box-shadow: none;
    }
  `;

  override update(changedProps: PropertyValues) {
    super.update(changedProps);

    if (changedProps.has('windows')) {
      this._windows = this._getUnshuffeledWindowsList(this.windows);
    }
  }

  _getUnshuffeledWindowsList(windowsList: Window[]): Window[] {
    return [...windows].map(
      ({ id }) => windowsList.find((window) => window.id === id)!
    );
  }

  override render() {
    return html`
      <section>
        ${repeat(
          this._windows,
          ({ id }) => id,
          (window: Window) =>
            html` ${window.visible
              ? html` <button
                  @click=${() => this._onClick(window)}
                  ?selected=${window.selected}
                  ?minimized=${window.minimized}
                >
                  <svg-icon .svg=${window.icon}></svg-icon>
                </button>`
              : nothing}`
        )}
      </section>
    `;
  }

  private _onClick(window: Window) {
    this.dispatchEvent(
      new CustomEvent('select', {
        detail: { window },
      })
    );
  }
}
