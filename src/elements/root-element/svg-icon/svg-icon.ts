import { css, LitElement, html, nothing, SVGTemplateResult } from 'lit';
import { property } from 'lit/decorators.js';
import { Size } from '../window-element/models/Size';

export class SvgIcon extends LitElement {
  @property() public svg!: SVGTemplateResult;
  @property() public size!: Size;

  static override styles = css`
    svg {
      width: 100%;
      height: 100%;
    }
  `;

  override render() {
    return html`
      ${this.size
        ? html`<style>
            ${css`
              svg {
                width: ${this.size.width}px;
                height: ${this.size.height}px;
              }
            `}
          </style>`
        : nothing}
      ${this.svg}
    `;
  }
}
