import { css } from 'lit';
import { background, primary, secondary } from '../../../styles/theme.styles';
import {
  materialBoxShadow1,
  materialBoxShadow2,
} from '../../../styles/fancy.styles';

export const styles = css`
  :host {
    position: relative;
  }

  section {
    position: absolute;

    display: flex;
    flex-direction: column;
    justify-content: stretch;

    border: 1px solid ${primary};
    box-sizing: border-box;
    border-radius: 4px;
    overflow: hidden;

    box-shadow: ${materialBoxShadow1};
    transition: box-shadow 250ms;
  }

  section[selected] {
    box-shadow: ${materialBoxShadow2};
  }

  section[hidden] {
    display: none;
  }

  header {
    flex: 0 0 auto;

    background: ${primary};
    border-bottom: 1px solid ${primary};
    cursor: default;
    user-select: none;
    padding: 3px;

    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  header > .left {
    display: flex;
    align-items: center;
    min-width: 0;
  }

  header > .left > .icon {
    height: 18px;
    width: 18px;
    margin: 0 6px 0 2px;
    box-sizing: border-box;
    flex: 0 0 auto;
  }

  header > .left > .title {
    color: white;
    font-size: 11px;
    margin-right: 4px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  header > .right {
    display: flex;
  }

  header > .right > button {
    border: 1px solid ${secondary};
    background: transparent;
    border-radius: 4px;
    height: 18px;
    width: 18px;
    padding: 0px;
    outline: none;
  }

  header > .right > button:hover {
    background: ${secondary};
  }

  header > .right > button:not(:last-child) {
    margin-right: 4px;
  }

  main {
    flex: 1 1 auto;

    position: relative;
    background: ${background};
    max-height: 100%;
    overflow: auto;
  }

  div.resize-handle {
    position: absolute;

    bottom: 0px;
    right: 0px;
    border-style: solid;
    border-width: 0 0 12px 12px;
    border-color: transparent transparent ${primary} transparent;
    cursor: nw-resize;
    user-select: none;
  }
`;
