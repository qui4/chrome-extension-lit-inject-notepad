import { ScopedElementsMixin } from '@open-wc/scoped-elements';
import { LitElement, html } from 'lit';
import { state, property, queryAsync } from 'lit/decorators.js';
import { nothing, SVGTemplateResult } from 'lit';
import { EventListenerMapping } from '../../../models/EventListenerMapping';
import { styles } from './window-element.styles';
import { close } from '../../../svg/light/navigation';
import { storage } from '../../../utils/local-storage';
import { SvgIcon } from '../svg-icon';
import { Position } from './models/Position';
import { Size } from './models/Size';

export class WindowElement extends ScopedElementsMixin(LitElement) {
  @property() public override id!: string;
  @property() public override title!: string;
  @property() public resizable!: boolean;
  @property() public minimized!: boolean;
  @property() public selected!: boolean;
  @property() public icon!: SVGTemplateResult;
  @property() public minSize: Size = {
    width: 250,
    height: 100,
  };
  @property() public maxSize: Size | undefined;

  /**
   * override the initial position (will be discarded if there is a saved position in localStorage)
   */
  @property() public initialPosition: Position = {
    x: 0,
    y: 0,
  };
  /**
   * override the initial position (will be discarded if there is a saved position in localStorage)
   */
  @property() public initialSize: Size | undefined;

  @queryAsync('section')
  private _windowElPromise!: Promise<HTMLElement>;

  @state() private _position: Position | undefined;
  @state() private _size: Size | undefined;

  private _eventListeners: EventListenerMapping[] = [];
  private _lastMousePosition: Position | undefined;
  private _hoveringOverButtons = false;

  set position(position: Position) {
    this._position = this._getPositionInBounds(position);
  }

  get position(): Position {
    return this._position || this.initialPosition;
  }

  set size(size: Size) {
    this._size = this._getSizeInBounds(size);
  }

  get size(): Size {
    return this._size || this.initialSize || this.minSize;
  }

  static get scopedElements() {
    return {
      'svg-icon': SvgIcon,
    };
  }

  static override styles = styles;

  override connectedCallback() {
    super.connectedCallback();

    this._windowElPromise.then(() => {
      // Set the initial position & size as soon as the window element is loaded
      this.size = this._getSavedSize();
      this.position = this._getSavedPosition();
      this._moveIntoViewport();
    });

    // Keep this window within the viewport on window resize
    this._eventListeners.push({
      target: window,
      type: 'resize',
      handler: () => {
        this._moveIntoViewport();
      },
    });

    this._eventListeners.push({
      target: this,
      type: 'mousedown',
      handler: () => {
        if (!this._hoveringOverButtons) {
          this.dispatchEvent(new CustomEvent('touched'));
        }
      },
    });

    this._eventListeners.forEach(({ target, type, handler }) =>
      target.addEventListener(type, handler)
    );
  }

  override disconnectedCallback() {
    super.disconnectedCallback();

    while (this._eventListeners.length) {
      const { target, type, handler } = this._eventListeners.pop()!;
      target.removeEventListener(type, handler);
    }
  }

  override render() {
    const windowStyle = `
      width: ${this._size?.width || 0}px;
      height: ${this._size?.height || 0}px;
      left: ${this._position?.x || 0}px;
      top: ${this._position?.y || 0}px;
    `;

    return html`
      <section
        style=${windowStyle}
        ?hidden=${this.minimized || !this._position}
        ?selected=${this.selected}
      >
        <header @mousedown=${this._onDragStart.bind(this)}>
          <div class="left">
            <div class="icon"><svg-icon .svg=${this.icon}></svg-icon></div>
            <span class="title"
              >${this.title || html`Window (${this.id})`}</span
            >
          </div>
          <div
            class="right"
            @mouseover=${() => (this._hoveringOverButtons = true)}
            @mouseout=${() => (this._hoveringOverButtons = false)}
          >
            <button @click=${this._onCloseWindow.bind(this)}>
              <svg-icon .svg=${close}></svg-icon>
            </button>
          </div>
        </header>
        <main>
          <slot @close=${this._onCloseWindow.bind(this)}>Loading...</slot>
        </main>
        ${this.resizable
          ? html`<div
              class="resize-handle"
              @mousedown=${this._onResizeStart.bind(this)}
            ></div>`
          : nothing}
      </section>
    `;
  }

  _savePosition(): void {
    storage.setItem(
      `window:${this.id}:position`,
      JSON.stringify(this.position)
    );
  }

  _getSavedPosition(): Position {
    return (
      JSON.parse(storage.getItem(`window:${this.id}:position`) as string) ||
      this.initialPosition || { x: 0, y: 0 }
    );
  }

  _saveSize(): void {
    storage.setItem(`window:${this.id}:size`, JSON.stringify(this.size));
  }

  _getSavedSize(): Size {
    return (
      JSON.parse(storage.getItem(`window:${this.id}:size`) as string) ||
      this.initialSize ||
      this.minSize
    );
  }

  _onCloseWindow() {
    this.dispatchEvent(new CustomEvent('minimize'));
  }

  _onDragStart(event: DragEvent) {
    this._lastMousePosition = {
      x: event.clientX,
      y: event.clientY,
    };

    // TODO: check if adding a debounce-timer helps with the movement delay
    document.onmousemove = this._onDrag.bind(this);
    document.onmouseup = this._onDragStop.bind(this);
  }

  _onDrag(event: MouseEvent) {
    const currentMousePosition: Position = {
      x: event.clientX,
      y: event.clientY,
    };

    const diff: Position = {
      x: currentMousePosition.x - this._lastMousePosition!.x,
      y: currentMousePosition.y - this._lastMousePosition!.y,
    };

    this.position = {
      x: this.position.x + diff.x,
      y: this.position.y + diff.y,
    };

    this._lastMousePosition = currentMousePosition;
  }

  _onDragStop() {
    document.onmousemove = null;
    document.onmouseup = null;
    this._savePosition();
  }

  _onResizeStart(event: DragEvent) {
    this._lastMousePosition = {
      x: event.clientX,
      y: event.clientY,
    };

    // TODO: check if adding a debounce-timer helps with the movement delay
    document.onmousemove = this._onResize.bind(this);
    document.onmouseup = this._onResizeStop.bind(this);
  }

  _onResize(event: MouseEvent) {
    const currentMousePosition: Position = {
      x: event.clientX,
      y: event.clientY,
    };

    const diff: Position = {
      x: currentMousePosition.x - this._lastMousePosition!.x,
      y: currentMousePosition.y - this._lastMousePosition!.y,
    };

    this.size = {
      width: this.size.width + diff.x,
      height: this.size.height + diff.y,
    };

    this._lastMousePosition = currentMousePosition;
  }

  _onResizeStop() {
    document.onmousemove = null;
    document.onmouseup = null;
    this._saveSize();
  }

  _getPositionInBounds(wantedPosition: Position): Position {
    if (!this._size) {
      return wantedPosition;
    }
    const { x, y } = wantedPosition;
    const { clientWidth, clientHeight } = document.documentElement;
    const { width, height } = this._size;
    const maxX = clientWidth - width;
    const maxY = clientHeight - height;

    const position = { ...wantedPosition };

    if (x < 0) {
      position.x = 0;
    } else if (x > maxX) {
      position.x = maxX;
    }

    if (y < 0) {
      position.y = 0;
    } else if (y > maxY) {
      position.y = maxY;
    }

    return position;
  }

  _getSizeInBounds(wantedSize: Size): Size {
    if (!this._position) {
      return wantedSize;
    }
    const { width, height } = wantedSize;
    const { clientWidth, clientHeight } = document.documentElement;
    const { x, y } = this._position;
    const maxSize: Size = this.maxSize || {
      width: clientWidth - x,
      height: clientHeight - y,
    };

    const size = { ...wantedSize };

    if (width < this.minSize.width) {
      size.width = this.minSize.width;
    } else if (width > maxSize.width) {
      size.width = maxSize.width;
    }

    if (height < this.minSize.height) {
      size.height = this.minSize.height;
    } else if (height > maxSize.height) {
      size.height = maxSize.height;
    }

    return size;
  }

  _moveIntoViewport(): void {
    // Call the setters for size & position with the wanted size & position
    this.position = this.position; // eslint-disable-line no-self-assign
    this.size = this.size; // eslint-disable-line no-self-assign

    // Save the new size & position in localstorage
    this._savePosition();
    this._saveSize();
  }
}
