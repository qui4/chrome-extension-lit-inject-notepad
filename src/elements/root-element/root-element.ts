import { LitElement, html, nothing } from 'lit';
import { state } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { ScopedElementsMixin } from '@open-wc/scoped-elements';
import { TaskbarElement } from './taskbar-element';
import { WindowElement } from './window-element';
import { Window } from '../../models/Window';
import { storage } from '../../utils/local-storage';
import { windows } from '../windows';
import { styles } from './root-element.styles';

export class RootElement extends ScopedElementsMixin(LitElement) {
  @state() private _windows: Window[];

  static get scopedElements() {
    return windows.reduce(
      (acc, { scopedElements }) => ({
        ...acc,
        ...scopedElements,
      }),
      {
        'taskbar-element': TaskbarElement,
        'window-element': WindowElement,
      }
    );
  }

  static override styles = styles;

  constructor() {
    super();
    this._windows = windows.map((window) => ({
      ...window,
      minimized: this._getSavedMinimizedState(window.id),
    }));
  }

  override render() {
    return this._windows
      ? html`
          ${repeat(
            this._windows,
            ({ id }) => id,
            ({
              id,
              title,
              visible,
              minimized,
              resizable,
              icon,
              minSize,
              template,
            }) =>
              visible
                ? html`<window-element
                    id=${id}
                    .title=${title!}
                    .minimized=${minimized}
                    .resizable=${resizable}
                    .icon=${icon}
                    .minSize=${minSize}
                    .selected=${this._isSelected(id)}
                    @touched=${() => this._selectWindow(id)}
                    @minimize=${() => this._setMinimizedState(id, true)}
                    >${template}</window-element
                  > `
                : nothing
          )}
          <taskbar-element
            .windows=${this._windows}
            @select=${({ detail }: CustomEvent) =>
              this._onTaskbarSelect(detail.window)}
          ></taskbar-element>
        `
      : nothing;
  }

  _setMinimizedState(windowId: string, to: boolean) {
    this._windows = [...this._windows].map((window) => {
      if (window.id === windowId) {
        window.minimized = to;
      }
      return window;
    });
    this._saveMinimizedState(windowId, to);
  }

  _saveMinimizedState(windowId: string, state: boolean): void {
    storage.setItem(`window:${windowId}:minimized`, state);
  }

  _getSavedMinimizedState(windowId: string): boolean {
    const minimizedState = storage.getItem(
      `window:${windowId}:minimized`
    ) as boolean;
    return typeof minimizedState === 'undefined' ? true : minimizedState;
  }

  _onTaskbarSelect({ id, minimized }: Window) {
    if (!this._isSelected(id)) {
      this._selectWindow(id);
      this._setMinimizedState(id, false);
    } else {
      this._setMinimizedState(id, !minimized);
    }
  }

  _selectWindow(windowId: string): void {
    const copy = [...this._windows].map((window) => {
      if (window.id === windowId) {
        window.selected = true;
      } else if (window.selected) {
        window.selected = false;
      }
      return window;
    });

    copy.forEach((window: Window, index: number) => {
      if (window.id === windowId) {
        copy.splice(index, 1);
        copy.push(window);
      }
    });

    this._windows = copy;
  }

  _isSelected(windowId: string) {
    return !!this._windows.find(
      ({ id, selected }) => id === windowId && selected
    );
  }
}
