import { LitElement, html } from 'lit';
import { storage } from '../../../utils/local-storage';
import { styles } from './settings-window.styles';

export class SettingsWindow extends LitElement {
  static override styles = styles;

  override render() {
    return html` <section>Nothing here yet!</section> `;
  }

  _save(setting: string, value: string | boolean): void {
    storage.setItem(`settings:${setting}`, value);
  }

  _getSaved(setting: string): string | boolean | null {
    return storage.getItem(`settings:${setting}`);
  }
}
