import { css } from 'lit';
import {
  background,
  foreground,
  secondary,
} from '../../../styles/theme.styles';

export const styles = css`
  :host,
  textarea {
    position: absolute;
    height: 100%;
    width: 100%;
  }

  textarea {
    border: 0px;
    padding: 0px;
    outline: none;
    background: ${background};
    color: ${foreground};
    resize: none;
  }

  section {
    position: absolute;
    bottom: 8px;
    right: 8px;
  }

  section button {
    border: none;
  }

  section button.primary {
    background: ${secondary};
  }
`;
