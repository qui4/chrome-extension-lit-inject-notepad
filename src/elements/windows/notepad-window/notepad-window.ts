import { LitElement, html, nothing } from 'lit';
import { storage } from '../../../utils/local-storage';
import { styles } from './notepad-window.styles';
import { state, query } from 'lit/decorators.js';

export class NotepadWindow extends LitElement {
  @state() private notesChanged = false;
  @state() private notes = '';

  @query('textarea') private textareaEl!: HTMLTextAreaElement;

  static override styles = styles;

  override connectedCallback() {
    super.connectedCallback();
    this.notes = this.getNotes();
  }

  override render() {
    return html`
      <textarea @keyup="${this.onChanged}">${this.notes}</textarea>
      ${this.notesChanged
        ? html`
            <section>
              <button @click="${this.revertNotes.bind(this)}">revert</button>
              <button class="primary" @click="${this.saveNotes.bind(this)}">
                save
              </button>
            </section>
          `
        : nothing}
    `;
  }

  private onChanged() {
    this.notesChanged = true;
  }

  private revertNotes(): void {
    this.textareaEl.value = this.getNotes();
    this.notesChanged = false;
  }

  private saveNotes(): void {
    storage.setItem('notes', this.textareaEl.value);
    this.notesChanged = false;
  }

  private getNotes(): string {
    return (storage.getItem('notes') || '').toString();
  }
}
