import { html } from 'lit';
import { Window } from '../../models/Window';
import { cherry, world } from '../../svg/icons';
import { SettingsWindow } from './settings-window';
import { NotepadWindow } from './notepad-window';

export const windows: Window[] = [
  {
    id: 'settings',
    title: 'Settings',
    scopedElements: { 'settings-window': SettingsWindow },
    template: html`<settings-window></settings-window>`,
    visible: true,
    icon: cherry,
    minSize: {
      width: 300,
      height: 500,
    },
  },
  {
    id: 'notepad',
    title: 'Notepad',
    scopedElements: { 'notepad-window': NotepadWindow },
    template: html`<notepad-window></notepad-window>`,
    visible: true,
    icon: world,
    minSize: {
      width: 200,
      height: 200,
    },
    resizable: true,
  },
];
