import commonjs from '@rollup/plugin-commonjs';
import filesize from 'rollup-plugin-filesize';
import copy from 'rollup-plugin-copy';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import typescript from '@rollup/plugin-typescript';

export default {
  input: 'src/main.ts',
  output: {
    file: 'extension/bundle.js',
    format: 'es',
    sourcemap: 'inline',
  },
  plugins: [
    nodeResolve(),
    typescript(),
    commonjs(),
    terser({
      module: true,
      warnings: true,
      mangle: {
        properties: { regex: /^__/ },
      },
    }),
    filesize(),
    copy({ targets: [{ src: 'public/*', dest: 'extension' }] }),
  ],
};
